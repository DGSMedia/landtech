# LandTech

## Application Structure

- client : React Front-end Application
- server : Nodejs Back-end REST API

## Docker setup

To run the application be sure to have Docker setup and running, then run :

`docker-compose up -d`

this will run the docker-compose.yml and deploy the application.

You can then access the application,
open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The REST API is available on port 3001
open [http://localhost:3001](http://localhost:3001) to view it in the browser.

## How it works

Once the application have finish to gather all the data from the backend a map should be visible.

Hover one of the grey box to see the house details and click on the grey box to select the house to start the comparison with the other.

This will update the houses with different colors based on how different is the price in % compare to the selected one.

0% - 5% --> "#58D68D"

5% - 25% --> "#F4D03F"

25% - 75% --> "#F5B041"

75% - 95% --> "#EB984E"

95% - 100% --> "#FF0000"

## Alternative Setup

#### Client

Install all the necessary packages

`npm i`

Launches the test runner in the interactive watch mode.

`npm run test`

Start the application [http://localhost:3000](http://localhost:3000)

`npm start`

Make an optimised production build

`npm run build`

#### Server setup

Install all the necessary packages

`npm i`

Start the REST API [http://localhost:3001](http://localhost:3001)

`npm start`

### TODO:

- Write back-end tests
- Handle error messages
- Add more front-end tests
- Add database to save dataset
- Style front-end
- Improve styled components
- Add cache layer on Backend side
- Think a better solution to improve performance of the dataset front-end side
