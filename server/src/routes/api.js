import { Router } from "express";
import { getDifference, getGroup } from "../lib/util";

export default data => {
  let api = Router();

  api.get("/", (req, res) => {
    res.send("server running");
  });

  api.get("/data", (req, res) => {
    res.send(data);
  });

  api.get("/data/:id", (req, res) => {
    const id = req.params.id;
    const selected = data.find(item => item.id === id);
    const response = data.map(item => {
      const newItem = { ...item };
      newItem.score = getDifference(item.price, selected.price);
      newItem.group = getGroup(newItem.score);
      return newItem;
    });
    res.send(response);
  });

  return api;
};
