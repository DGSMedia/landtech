const axios = require("axios");

export async function getDataSet() {
  let dataSet = [];
  const fileData = await getFileData(
    "https://raw.githubusercontent.com/landtechnologies/technical-challenge/master/sold-price-data.txt"
  );
  if (fileData) {
    dataSet = getFormatedData(fileData);
  }
  return dataSet;
}

export async function getFileData(url) {
  try {
    const response = await axios.get(url);
    return response.data;
  } catch (error) {
    return null;
  }
}

export function getFormatedData(fileData) {
  const items = fileData.split(/\n/).filter(Boolean);
  const formatedItems = items.map((ItemString, index) => {
    const item = ItemString.split(" ");
    return {
      id: (index + 1).toString(),
      x: parseInt(item[0]),
      y: parseInt(item[1]),
      price: parseInt(item[2])
    };
  });
  return formatedItems;
}

export function getDifference(v1, v2) {
  // return Math.abs(((v1 - v2) / ((v1 + v2) / 2)) * 100); // if absolute value is required
  return ((v1 - v2) / ((v1 + v2) / 2)) * 100;
}

export function getGroup(score) {
  if (score < 5) return "1";
  if (score < 25) return "2";
  if (score < 75) return "3";
  if (score < 95) return "4";
  return "5";
}
