module.exports = {
  extends: ["airbnb", "prettier"],
  parser: "babel-eslint",
  env: {
    jest: "true",
    browser: "true"
  },
  plugins: ["prettier"],
  rules: {
    "prettier/prettier": "warn",
    "react/jsx-filename-extension": "off",
    "react/jsx-indent": "off",
    "import/no-extraneous-dependencies": "off",
    "react/forbid-foreign-prop-types": "off",
    "react/prop-types": "off",
    "react/jsx-one-expression-per-line": "off",
    "import/no-cycle": "off",
    "jsx-a11y/label-has-for": "off",
    semi: [2, "never"]
  }
}
