import http from "http";
import express from "express";
import cors from "cors";
import config from "./src/config.json";
import routes from "./src/routes";
import { getDataSet } from "./src/lib/util";

let app = express();

app.server = http.createServer(app);

app.use(cors({ exposedHeaders: config.corsHeaders }));

async function Start() {
  const dataSet = await getDataSet();
  app.use("/api", routes.api(dataSet));
  app.server.listen(process.env.PORT || config.port, () => {
    console.log(`Started on port ${app.server.address().port}`);
  });
}

Start();



