import { createStore, combineReducers, applyMiddleware, compose } from "redux"
import createSagaMiddleware from "redux-saga"
import { spawn } from "redux-saga/effects"

import { HouseMapReducer, HouseMapSaga } from "./HouseMap.module"

const sagaMiddleware = createSagaMiddleware()

const rootReducer = combineReducers({
  houseMap: HouseMapReducer
})

/* eslint-disable no-underscore-dangle */
const enhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const store = createStore(
  rootReducer,
  enhancers(applyMiddleware(sagaMiddleware))
)
/* eslint-enable */

function* rootSaga() {
  yield spawn(HouseMapSaga)
}

sagaMiddleware.run(rootSaga)

export default store
