import React from "react"
import { HouseMapContainer } from "./HouseMap.module"

const App = () => (
  <div className="App">
    <HouseMapContainer />
  </div>
)

export default App
