import React from "react"
import ReactDOM from "react-dom"
import { ThemeProvider, createGlobalStyle } from "styled-components"
import { Provider as StoreProvider } from "react-redux"

import App from "./App"
import store from "./App.store"

const GlobalStyles = createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500|Roboto+Slab:300,400,700');

  * {
    box-sizing: border-box;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
  }

  body {
    margin: 0;
    padding: 0;
  }

  a {
    color: inherit;
    text-decoration: inherit;
  }
`

const theme = {
  main: "mediumseagreen"
}

ReactDOM.render(
  <StoreProvider store={store}>
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyles />
        <App />
      </>
    </ThemeProvider>
  </StoreProvider>,
  document.getElementById("root")
)
