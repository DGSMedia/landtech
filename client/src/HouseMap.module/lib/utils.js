import { useRef, useState, useEffect } from "react"

export const getColorGroup = group => {
  if (group === "1") return "#58D68D"
  if (group === "2") return "#F4D03F"
  if (group === "3") return "#F5B041"
  if (group === "4") return "#EB984E"
  if (group === "5") return "#FF0000"
  return "grey"
}

export function useHover() {
  const [value, setValue] = useState(false)

  const ref = useRef(null)

  const handleMouseOver = () => setValue(true)
  const handleMouseOut = () => setValue(false)

  useEffect(() => {
    const node = ref.current
    if (node) {
      node.addEventListener("mouseover", handleMouseOver)
      node.addEventListener("mouseout", handleMouseOut)

      return () => {
        node.removeEventListener("mouseover", handleMouseOver)
        node.removeEventListener("mouseout", handleMouseOut)
      }
    }
  }, [])

  return [ref, value]
}
