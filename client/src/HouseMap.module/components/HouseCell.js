import React from "react"
import styled from "styled-components"
import Cell from "./Cell"

const EmptyCell = styled.div`
  position: relative;
  height: 10px;
  min-width: 10px;
  background: white;
`

const HouseCell = ({ dataset, selectId, selectedHouse, x, y }) => {
  const house = dataset.find(house => house.x === x && house.y === y)
  if (!house) return <EmptyCell />
  return <Cell data={house} selectId={selectId} selectedHouse={selectedHouse} />
}

export default HouseCell
