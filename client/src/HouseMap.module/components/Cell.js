import React from "react"

import styled from "styled-components"
import { getColorGroup, useHover } from "../lib/utils"

const BoardCell = styled.div`
  position: relative;
  height: 10px;
  min-width: 10px;
  justify-content: center;
  align-items: center;
  font-size: 10px;
  background: ${props =>
    props.selected ? "#000" : getColorGroup(props.group)};
  border: ${props =>
    props.isHovered && props.group ? "3px solid #ff00ff" : "0px solid #ccc;"};
`
const InfoBox = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  width: 120px;
  background-color: black;
  color: #fff;
  border-radius: 6px;
  padding: 5px;
  position: absolute;
  left: 10px;
  z-index: 1;
  font-size: 12px;
`

const Cell = ({ data, selectId, selectedHouse }) => {
  const [hoverRef, isHovered] = useHover()
  return (
    <BoardCell
      ref={hoverRef}
      isHovered={isHovered}
      group={data.group}
      selected={selectedHouse.id === data.id}
      onClick={e => (data.id ? selectId(data.id) : e)}
    >
      {data.group && isHovered && (
        <InfoBox>
          <span>id: {data.id}</span>
          <span>
            X: {data.x} Y: {data.y}
          </span>
          <span>Price: {data.price}</span>
          <span>Score: {data.score.toFixed(2)} %</span>
        </InfoBox>
      )}
    </BoardCell>
  )
}

export default Cell
