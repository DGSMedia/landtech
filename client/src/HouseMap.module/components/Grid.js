import React from "react"

import styled from "styled-components"
import HouseCell from "../containers/HouseCell"

const Board = styled.div`
  display: flex;
  flex-direction: column;
  flex-wrap: wrap;
  width: 100%;
  border: 1px solid blue;
`
const Row = styled.div`
  display: flex;
  flex-direction: row;
`

const Grid = () => {
  const rows = []
  for (let r = 0; r <= 100; r++) {
    const cells = []
    for (let c = 0; c <= 100; c++) {
      cells.push(c)
    }
    rows.push(cells)
  }

  return (
    <Board>
      {rows.map((rows, rowIndex) => (
        <Row key={rowIndex.toString()}>
          {rows.map(value => (
            <HouseCell
              key={value.toString()}
              x={value}
              y={rowIndex}
            ></HouseCell>
          ))}
        </Row>
      ))}
    </Board>
  )
}

export default Grid
