import React, { useEffect } from "react"
import styled from "styled-components"
import Grid from "./Grid"
import Spinner from "../../components/Spinner"

const Page = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  width: 100vw;
  height: 100vh;
  border: 1px solid blue;
`

const HouseMap = ({ isLoading, onLoad, selectId }) => {
  useEffect(() => {
    onLoad(selectId)
  }, [])

  if (isLoading)
    return (
      <Page>
        <Spinner />
        Loading ...
      </Page>
    )
  return <Grid />
}

export default HouseMap
