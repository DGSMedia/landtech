import { createSelector } from "reselect"

const houseMap = state => state.houseMap

export const getIsLoading = createSelector(
  houseMap,
  state => state.isLoading
)

export const getSelectedHouse = createSelector(
  houseMap,
  state => state.dataset.find(house => house.id === state.selectedId) || {}
)

export const getSelectedId = createSelector(
  houseMap,
  state => state.selectedId
)

export const getDataset = createSelector(
  houseMap,
  state => state.dataset
)
