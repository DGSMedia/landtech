import { types } from "./housemap.actions"

export const initialState = {
  isLoading: false,
  selectedId: null,
  selectedHouse: null,
  dataset: []
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.LOAD:
      return { ...state, isLoading: true }

    case types.SELECT_ID:
      return { ...state, selectedId: action.payload.id, isLoading: true }

    case types.LOAD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        dataset: action.payload.dataset
      }

    case types.LOAD_FAIL:
      return { ...state, isLoading: false }

    default:
      return state
  }
}
