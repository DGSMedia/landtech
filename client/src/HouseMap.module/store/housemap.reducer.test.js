import * as Actions from "./houseMap.actions"
import { reducer, initialState } from "./housemap.reducer"

const DATASET = []

const LOADING_STATE = { ...initialState, isLoading: true }

describe("[HouseMap] Reducer", () => {
  let result

  describe("Loading the dataset Result", () => {
    it("Sets the loading flag to true", () => {
      result = reducer(initialState, Actions.load())
      expect(result.isLoading).toBeTruthy()
    })

    describe("Successful Loading", () => {
      beforeEach(() => {
        result = reducer(
          LOADING_STATE,
          Actions.loadSuccess({
            dataset: DATASET
          })
        )
      })

      it("Sets the loading flag to false", () => {
        expect(result.isLoading).toBeFalsy()
      })

      it("Stores the dataset", () => {
        expect(result.dataset).toEqual(DATASET)
      })
    })

    describe("Unsuccessful Loading", () => {
      it("Sets the loading flag to false", () => {
        result = reducer(LOADING_STATE, Actions.loadFail())
        expect(result.isLoading).toBeFalsy()
      })
    })
  })

  describe("By Default", () => {
    it("Does not modify the current state", () => {
      result = reducer(initialState, {})
      expect(result).toEqual(initialState)
    })
  })
})
