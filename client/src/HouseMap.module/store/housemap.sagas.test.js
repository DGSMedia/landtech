import { expectSaga } from "redux-saga-test-plan"

import { loadDataset } from "./housemap.sagas"
import { load, loadSuccess, loadFail } from "./housemap.actions"

const DATASET = []

const RESPONSE = {
  data: DATASET
}

const API_MOCK = jest.fn(async () => RESPONSE)
const API_MOCK_FAILED = jest.fn(async () => new Error("request failed"))

describe("[HouseMap] Sagas", () => {
  describe("#loadDataset", () => {
    it("Retrieves the dataset from the API", async () =>
      expectSaga(loadDataset, load(), API_MOCK)
        .put(
          loadSuccess({
            dataset: DATASET
          })
        )
        .dispatch(load())
        .run())
  })

  it("Dispatches a loadFail if the request fails", async () =>
    expectSaga(loadDataset, load(), API_MOCK_FAILED)
      .put(loadFail())
      .dispatch(load())
      .run())
})
