export const types = {
  LOAD: "[HouseMap] Load",
  LOAD_SUCCESS: "[HouseMap] Load Success",
  LOAD_FAIL: "[HouseMap] Load Fail",
  SELECT_ID: "[HouseMap] Select id"
}

export const load = id => ({
  type: types.LOAD,
  payload: {
    id
  }
})

export const selectId = id => ({
  type: types.SELECT_ID,
  payload: {
    id
  }
})

export const loadSuccess = ({ dataset }) => ({
  type: types.LOAD_SUCCESS,
  payload: {
    dataset
  }
})

export const loadFail = () => ({
  type: types.LOAD_FAIL
})
