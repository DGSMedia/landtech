import * as Selectors from "./housemap.selectors"

const DATASET = []

const STATE = {
  houseMap: {
    dataset: DATASET
  }
}

describe("[HouseMap] Selectors", () => {
  let result

  describe("#getDataset", () => {
    it("Returns the dataset ", () => {
      result = Selectors.getDataset(STATE)
      expect(result).toEqual(DATASET)
    })
  })
})
