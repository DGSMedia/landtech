import { takeLatest, put, call } from "redux-saga/effects"

import * as Actions from "./housemap.actions"
import load from "../services/housemap.service"

export function* loadDataset(action, api = load) {
  const res = yield call(api, action.payload)
  try {
    const dataset = res.data
    if (dataset) {
      yield put(
        Actions.loadSuccess({
          dataset
        })
      )
    } else yield put(Actions.loadFail())
  } catch (e) {
    console.log(e)
    yield put(Actions.loadFail())
  }
}

export function* saga() {
  yield takeLatest(Actions.types.LOAD, loadDataset)
  yield takeLatest(Actions.types.SELECT_ID, loadDataset)
}
