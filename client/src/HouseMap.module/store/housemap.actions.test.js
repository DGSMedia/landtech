import * as Actions from "./houseMap.actions"

const DATASET = {
  score: 500,
  scoreMax: 1000,
  percentage: 50
}

describe("[HouseMap] Actions", () => {
  let result

  describe("#load", () => {
    it("Creates a load action for the houseMap reducer to consume", () => {
      result = Actions.load()
      expect(result.type).toBe(Actions.types.LOAD)
    })
  })

  describe("#loadSuccess", () => {
    beforeEach(() => {
      result = Actions.loadSuccess({
        dataset: DATASET
      })
    })

    it("Creates a loadSuccess action for the houseMap reducer to consume", () => {
      expect(result.type).toBe(Actions.types.LOAD_SUCCESS)
    })

    it("Includes the houseMap dataset in the action's payload", () => {
      expect(result.payload).toEqual({
        dataset: DATASET
      })
    })
  })

  describe("#loadFail", () => {
    it("Creates a load fail action for the houseMap reducer to consume", () => {
      result = Actions.loadFail()
      expect(result.type).toBe(Actions.types.LOAD_FAIL)
    })
  })
})
