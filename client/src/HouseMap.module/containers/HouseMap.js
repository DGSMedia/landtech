import { connect } from "react-redux"
import { load } from "../store/housemap.actions"
import { getIsLoading, getSelectedId } from "../store/housemap.selectors"

import HouseMap from "../components/HouseMap"

const mapState = state => ({
  isLoading: getIsLoading(state),
  selectId: getSelectedId(state)
})

const mapDispatch = dispatch => ({
  onLoad: id => dispatch(load(id))
})

const HouseMapContainer = connect(
  mapState,
  mapDispatch
)(HouseMap)

export default HouseMapContainer
