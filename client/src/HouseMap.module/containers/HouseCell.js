import { connect } from "react-redux"
import { selectId } from "../store/housemap.actions"
import { getDataset, getSelectedHouse } from "../store/housemap.selectors"

import HouseCell from "../components/HouseCell"

const mapState = state => ({
  dataset: getDataset(state),
  selectedHouse: getSelectedHouse(state)
})

const mapDispatch = dispatch => ({
  selectId: id => dispatch(selectId(id))
})

const HouseCellContainer = connect(
  mapState,
  mapDispatch
)(HouseCell)

export default HouseCellContainer
