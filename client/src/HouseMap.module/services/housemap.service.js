import axios from "axios"

const API_URL = `http://localhost:3001/api/data/`

const load = ({ id }, url = API_URL) => {
  if (id) return axios.get(`${url}${id}`)
  else return axios.get(url)
}

export default load
